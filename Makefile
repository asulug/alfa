all: alfa.pdf

alfa.pdf: alfa.tex
	latexmk -pdf $<

alfa.png: alfa.pdf
	convert -density 150 -flatten $< $@

clean: alfa.tex
	latexmk -CA $<

.PHONY: all clean alfa.pdf
